//
//  DhtButtonView.m
//  Cosplay No.1
//
//  Created by nguyenvandung on 10/10/15.
//  Copyright © 2015 Framgia. All rights reserved.
//

#import "DhtButtonView.h"
@interface DhtButtonView()
{
    NSString *_title;
    NSArray *_possibleTitles;
    long _backgroundImageFingerprint;
    long _highightedBackgroundImageFingerprint;
    long _titleFontFingerprint;
    
    long _imageFingerprint;
}
@property (nonatomic, strong) NSString *viewIdentifier;
@property (nonatomic, strong) NSString *viewStateIdentifier;
@end
@implementation DhtButtonView

- (id) init {
    self = [super init];
    if (self) {
        self.adjustsImageWhenDisabled = false;
        self.adjustsImageWhenHighlighted = false;
        self.userInteractionEnabled = false;
    }
    return self;
}

- (BOOL)hidden {
    return self.isHidden;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsLayout];
}

- (void)willBecomeRecycled {
    self.viewStateIdentifier = nil;
    self.backgroundColor = nil;
    [self removeFromSuperview];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImageFingerprint = (long)backgroundImage;
    [self setBackgroundImage:backgroundImage forState:UIControlStateNormal];
}

- (void)setHighlightedBackgroundImage:(UIImage *)highlightedBackgroundImage {
    _highightedBackgroundImageFingerprint = (long)highlightedBackgroundImage;
    [self setBackgroundImage:highlightedBackgroundImage forState:UIControlStateHighlighted];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self setTitle:title forState:UIControlStateNormal];
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFontFingerprint = (long)titleFont;
    self.titleLabel.font = titleFont;
}

- (void)setImage:(UIImage *)image {
    _imageFingerprint = (long)image;
    [self setImage:image forState:UIControlStateNormal];
}

- (NSString *)viewStateIdentifier {
    return [[NSString alloc] initWithFormat:@"ButtonView/%lx/%lx/%@/%lx/%lx", _backgroundImageFingerprint, _highightedBackgroundImageFingerprint, _title, (long)_titleFontFingerprint, (long)_imageFingerprint];
}

- (void) setViewIdentifier:(NSString *)viewIdentifier {
    _viewIdentifier = viewIdentifier;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (!UIEdgeInsetsEqualToEdgeInsets(_extendedEdges, UIEdgeInsetsZero)) {
        CGRect extendedFrame = self.bounds;
        extendedFrame.origin.x -= _extendedEdges.left;
        extendedFrame.size.width += _extendedEdges.left;
        extendedFrame.origin.y -= _extendedEdges.top;
        extendedFrame.size.height += _extendedEdges.top;
        
        extendedFrame.size.width += _extendedEdges.right;
        extendedFrame.size.height += _extendedEdges.bottom;
        
        if (CGRectContainsPoint(extendedFrame, point))
            return self;
    }
    return [super hitTest:point withEvent:event];
}
@end
