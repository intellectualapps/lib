//
//  KLogger.h
//  kids_taxi
//
//  Created by Nguyen Van Dung on 4/28/16.
//  Copyright © 2016 JapanTaxi. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface KLogger : NSObject
+ (void)log:(NSString *)message;
@end
