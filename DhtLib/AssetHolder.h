//
//  AssetHolder.h
//  Cosplay No.1
//
//  Created by nguyen van dung on 11/5/15.
//  Copyright © 2015 Framgia. All rights reserved.
//



#import <Foundation/Foundation.h>
#import "PostNewObject.h"

@class AssetHolder;
#ifdef __cplusplus
extern "C" {
#endif
    
    AssetHolder *assetHolder();
    
#ifdef __cplusplus
}
#endif

@interface AssetHolder : NSObject

@property (nonatomic, assign) NSInteger     maxItems;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, copy) void (^getPhotoCompletion)(NSArray *pickerAssets);
@property (nonatomic, copy) void (^cancel)();

- (void)clear;
- (void)prepareForUse;
- (void)removeAssetWithUrl:(NSString *)url;
- (BOOL)assetIsExistedWithUrl:(NSString *)url;
- (void)removeObjectAtIndex:(NSInteger)index;
- (void)executeCallback;
- (BOOL)isFull;
@end
