//
//  DhtChatAsset.h
//  DhtChat
//
//  Created by Nguyen Van Dung on 10/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol DhtChatSourceAssetProtocol<NSObject>
@required
- (UIImage *)startIcon:(BOOL)isFavourite;
- (UIImage *)checkIcon:(BOOL)isChecked;
- (UIImage *)playImage:(BOOL)incomming;
- (UIImage *)downloadImage:(BOOL)incomming;
- (UIImage *)pauseImage:(BOOL)incomming;
- (UIImage *)cancelImage:(BOOL)incomming;
- (UIImage *)incomingTextBackgroundImage;
- (UIImage *)audioMessageBackgroundImage;
- (UIImage *)incomingTemplateTextBackgroundImage;
- (UIImage *)incomingTemplateTextBackgroundImageSelected;
- (UIImage *)outgoingTextBackgroundImage;
- (UIImage *)sentFailButtonImage;
- (UIImage *)defaultAvatarImage;
- (UIImage *)messageLinkFull;
- (UIImage *)messageLinkCornerLR;
- (UIImage *)messageLinkCornerBT;
- (UIImage *)messageLinkCornerRL;
- (UIImage *)messageLinkCornerTB;
- (UIImage *)arrowRightButton;
@end
@interface DhtChatAsset : NSObject<DhtChatSourceAssetProtocol>
+ (DhtChatAsset *) instance;
@end
