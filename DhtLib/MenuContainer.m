//
//  MenuContainer.m
//  MtoM
//
//  Created by Nguyen Van Dung on 3/31/16.
//  Copyright © 2016 Framgia. All rights reserved.
//

#import "MenuContainer.h"
#import "MenuView.h"

@implementation MenuContainer

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self != nil) {
        _menuView = [[MenuView alloc] init];
        [self addSubview:_menuView];
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *result = [super hitTest:point withEvent:event];
    if (result == self || result == nil) {
        [self hideMenu];
        return nil;
    }
    return result;
}

- (void)showMenuFromRect:(CGRect)rect {
    _isShowingMenu = true;
    _showingMenuFromRect = rect;
    [_menuView showInView:self fromRect:rect];
}

- (void)setFrame:(CGRect)frame {
    if (!CGSizeEqualToSize(frame.size, self.frame.size)) {
        [self hideMenu];
    }
    [super setFrame:frame];
}

- (void)hideMenu {
    if (_isShowingMenu) {
        _isShowingMenu = false;
        _showingMenuFromRect = CGRectZero;
        if ([_menuView completion]) {
            _menuView.completion(@"menuWillHide", nil);
        }
        
        [_menuView hide:^{
             [self removeFromSuperview];
        }];
    }
}
@end
