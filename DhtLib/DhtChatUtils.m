//
//  DhtChatUtils.m
//  DhtChat
//
//  Created by Nguyen Van Dung on 10/19/16.
//  Copyright © 2016 Dht. All rights reserved.
//

#import "DhtChatUtils.h"
#import <CoreText/CoreText.h>
CGFloat DhtScreenScaling() {
    static CGFloat value = 2.0f;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        value = [UIScreen mainScreen].scale;
    });

    return value;
}

UIColor* colorRGB(int rgb, float alpha) {
    return ([[UIColor alloc] initWithRed: (((rgb >> 16) & 0xff) / 255.0f)
                                   green: (((rgb >> 8) & 0xff) / 255.0f)
                                    blue: (((rgb) & 0xff) / 255.0f)
                                   alpha: alpha]);
}


@implementation DhtChatUtils

+ (NSArray *)textCheckingResults:(NSString *)_text {
    if (_text.length < 3) {
        return nil;
    }
    bool containsSomething = false;

    const unichar *stringCharacters = CFStringGetCharactersPtr((__bridge CFStringRef)_text);
    NSInteger length = _text.length;

    int digitsInRow = 0;
    int schemeSequence = 0;
    int dotSequence = 0;

    unichar lastChar = 0;

    if (stringCharacters != NULL) {
        for (int i = 0; i < length; i++) {
            unichar c = stringCharacters[i];
            if (c >= '0' && c <= '9') {
                digitsInRow++;
                if (digitsInRow >= 6) {
                    containsSomething = true;
                    break;
                }
                schemeSequence = 0;
                dotSequence = 0;
            } else if (c == ':') {
                if (schemeSequence == 0) {
                    schemeSequence = 1;
                } else {
                    schemeSequence = 0;
                }
            } else if (c == '/') {
                if (schemeSequence == 2) {
                    containsSomething = true;
                    break;
                }

                if (schemeSequence == 1) {
                    schemeSequence++;
                } else {
                    schemeSequence = 0;
                }
            } else if (c == '.') {
                if (dotSequence == 0 && lastChar != ' ') {
                    dotSequence++;
                } else {
                    dotSequence = 0;
                }
            } else if (c != ' ' && lastChar == '.' && dotSequence == 1) {
                containsSomething = true;
                break;
            }
            lastChar = c;
        }
    } else {
        SEL sel = @selector(characterAtIndex:);
        unichar (*characterAtIndexImp)(id, SEL, NSUInteger) = (typeof(characterAtIndexImp))[_text methodForSelector:sel];

        for (int i = 0; i < length; i++) {
            unichar c = characterAtIndexImp(_text, sel, i);
            if (c >= '0' && c <= '9') {
                digitsInRow++;
                if (digitsInRow >= 6) {
                    containsSomething = true;
                    break;
                }
                schemeSequence = 0;
                dotSequence = 0;
            }
            else if (!(c != ' ' && digitsInRow > 0))
                digitsInRow = 0;

            if (c == ':') {
                if (schemeSequence == 0)
                    schemeSequence = 1;
                else
                    schemeSequence = 0;
            } else if (c == '/') {
                if (schemeSequence == 2) {
                    containsSomething = true;
                    break;
                }

                if (schemeSequence == 1)
                    schemeSequence++;
                else
                    schemeSequence = 0;
            } else if (c == '.') {
                if (dotSequence == 0 && lastChar != ' ')
                    dotSequence++;
                else
                    dotSequence = 0;
            } else if (c != ' ' && lastChar == '.' && dotSequence == 1) {
                containsSomething = true;
                break;
            } else {
                dotSequence = 0;
            }

            lastChar = c;
        }
    }

    if (containsSomething) {
        NSError *error = nil;
        static NSDataDetector *dataDetector = nil;
        if (dataDetector == nil)
            dataDetector = [NSDataDetector dataDetectorWithTypes:(int)(NSTextCheckingTypeLink | NSTextCheckingTypePhoneNumber) error:&error];

        NSMutableArray *results = [[NSMutableArray alloc] init];
        [dataDetector enumerateMatchesInString:_text options:0 range:NSMakeRange(0, _text.length) usingBlock:^(NSTextCheckingResult *match, __unused NSMatchingFlags flags, __unused BOOL *stop)
         {
             NSTextCheckingType type = [match resultType];
             if (type == NSTextCheckingTypeLink || type == NSTextCheckingTypePhoneNumber)
             {
                 [results addObject:match];
             }
         }];
        return results;
    }

    return nil;
}

+ (NSArray *)additionAttributeForUrls:(NSArray *)checkingResult {
    if (checkingResult == nil || checkingResult.count == 0) {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    for (NSTextCheckingResult *result  in checkingResult) {
        NSArray *fontAttributes = [[NSArray alloc] initWithObjects:(__bridge id)[UIColor whiteColor].CGColor, (NSString *)kCTForegroundColorAttributeName, @(kCTUnderlineStyleNone|kCTUnderlineStyleSingle), (NSString *)NSUnderlineStyleAttributeName,(__bridge id)[UIColor redColor].CGColor, NSUnderlineColorAttributeName, nil];

        NSRange range = NSMakeRange(result.range.location, result.range.length);
        [array addObjectsFromArray:[NSArray arrayWithObjects:[[NSValue alloc] initWithBytes:&range objCType:@encode(NSRange)],fontAttributes, nil]];
    }
    return array;
}

+ (CGContextRef)createContentContext:(CGSize)size {
    CGSize contextSize = size;
    CGFloat scaling = DhtScreenScaling();

    contextSize.width *= scaling;
    contextSize.height *= scaling;

    size_t bytesPerRow = 4 * (int)contextSize.width;
    bytesPerRow = (bytesPerRow + 15) & ~15;

    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Host;

    CGContextRef context = CGBitmapContextCreate(NULL, (int)contextSize.width, (int)contextSize.height, 8, bytesPerRow, colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);

    CGContextScaleCTM(context, scaling, scaling);

    return context;
}

+ (CGSize) sizeForText:(NSString *)text
             boundSize:(CGSize)bsize
                option:(NSStringDrawingOptions)option
         lineBreakMode:(NSLineBreakMode)lineBreakMode
                  font:(UIFont *)font {
    if (!text || text.length ==0 || !font) {
        return CGSizeZero;
    }
    CGSize textSize = CGSizeZero;
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = lineBreakMode;

    textSize = [text boundingRectWithSize: bsize
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:font,NSParagraphStyleAttributeName:paragraphStyle}
                                  context:nil].size;
    return textSize;
}

+ (NSString *)generateUUID {
    NSString *result = nil;
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    if (uuid) {
        result = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
    }
    return result;
}

+ (NSString *)tempPath {
    NSString *str=  [NSTemporaryDirectory() stringByAppendingPathComponent:@"app"];
    BOOL isDir;
    if (![[NSFileManager defaultManager] fileExistsAtPath:str isDirectory:&isDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return str;
}

+ (NSString *)documentPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

+ (NSString *)recordedPath {
    NSString *str=  [[DhtChatUtils documentPath] stringByAppendingPathComponent:@"recorded"];
    BOOL isDir;
    if (![[NSFileManager defaultManager] fileExistsAtPath:str isDirectory:&isDir]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return str;
}
@end
