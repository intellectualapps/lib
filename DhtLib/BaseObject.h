//
//  BaseObject.h
//  DhtCommonLib
//
//  Created by Nguyen Van Dung on 2/24/16.
//  Copyright © 2016 Nguyen Van Dung. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BaseObject

@optional
- (NSString *) compareKey;
- (NSString *) groupKey;
- (NSString *) objectId;

@end