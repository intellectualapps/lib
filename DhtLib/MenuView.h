//
//  MenuView.h
//  MtoM
//
//  Created by Nguyen Van Dung on 3/31/16.
//  Copyright © 2016 Framgia. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^MenuActionCompletion)(NSString *type, NSDictionary *options);
@interface MenuView : UIView
- (void)setUserInfo:(NSDictionary *)userInfo;
- (void)setButtonsAndActions:(NSArray *)buttonsAndActions completion:(MenuActionCompletion)completion;
- (void)showInView:(UIView *)view fromRect:(CGRect)rect;
@property (nonatomic, copy) MenuActionCompletion completion;
- (void)hide:(dispatch_block_t)completion;
@end
