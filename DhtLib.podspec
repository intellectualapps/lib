Pod::Spec.new do |s|
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "DhtLib"
s.summary = "DhtLib is common libray for some tasks"
s.requires_arc = true
s.version = "2.0"
s.license = { :type => "MIT", :file => "LICENSE" }
s.author = { "note1" => "note2" }
s.homepage = "http://tracymoongiftcardstore.com"
s.source = { :git => "https://github.com/nguyenvandungb/DhtLib.git", :tag => "#{s.version}"}
s.framework = "UIKit"
s.framework = "Foundation"
s.framework = "ImageIO"
s.framework = "MobileCoreServices"
s.source_files = 'DhtLib/*.{h,m,mm}'

end
